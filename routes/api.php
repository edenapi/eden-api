<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('all-posts', 'PostsController@index');
Route::get('upvote/{id}', 'PostsController@upvote')->where('id', '[0-9]+');



Route::group([

    'middleware' => 'api',

], function ($router) {

    Route::get('get-token/{name}', ['as' => 'get-token', 'uses' => 'AuthController@getToken'])->where('name', '[a-z0-9]+');
    Route::post('new-post', 'PostsController@store');
    Route::get('delete/{id}', 'PostsController@delete')->where('id', '[0-9]+');
});
