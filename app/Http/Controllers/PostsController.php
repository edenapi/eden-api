<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use Validator;
use JWTFactory;
use JWTAuth;
use URL;
use DB;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    private $query;

    public function __construct()
    {
        $url = URL::to("/");
        $this->query =  Posts::select(
            'posts.id',
            'authors.author',
            'title',
            'content',
            'date_created',
            'votes',
            DB::raw(
                "CASE
                    WHEN (image is null) THEN image
                    ELSE CONCAT( '" . $url . "' , image)
                END as image"
            )
        )
            ->leftJoin('authors', 'authors.id', '=', 'posts.author');;
    }

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_by' => 'alpha',
            'sort' => 'alpha',
            'search' => 'regex:/^[a-zA-Z0-9 -.,;@()]+$/',
            
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_errors',
                "message" => $validator->errors(),
            ], 422);
        }

        $sort = 'date_created';
        $order  = 'desc';

        if ($request->has('order_by')) {
            $by = $request->input('order_by');
            if ($by == 'date')
                $sort = 'date_created';
            else if ($by == 'votes')
                $sort = 'votes';
            else
                return response()->json([
                    "error" => "error",
                    "message" => "Unable to sort posts"
                ], 500);
        }
        if ($request->has('sort')) {
            $type = $request->input('sort');
            if ($type == 'newest' || $type == 'most')
                $order = 'desc';
            else if ($type == 'oldest' || $type == 'least')
                $order = 'asc';
            else
                return response()->json([
                    "error" => "error",
                    "message" => "Unable to sort posts"
                ], 500);
        }

        $query = $this->query->orderBy($sort, $order);


        if ($request->has('search')) {
            $term = $request->input('search');
            $query = $query->where('authors.author', 'like', '%' . $term . '%')
                ->orWhere('content', 'like', '%' . $term . '%')
                ->orWhere('title', 'like', '%' . $term . '%');
        } 

        $posts  = $query->get();

        if (count($posts) == 0)
            return response()->json([
                "error" => "error",
                "message" => "There are no results matching the criteria"
            ], 500);

        return response()->json(['posts'=> $posts], 200);
    }

    public function delete($id)
    {
        $user = auth()->user();
        $post = Posts::find($id);
        if ($user && $user->id == $post->author) {
            try {

                $post->delete();
                return response()->json(['status' => 'Successfully deleted post'], 200);
            } catch (Exception $e) {
                return response()->json([
                    "error" => "error",
                    "message" => "Unable to delete a post"
                ], 500);
            }
        } else
            return response()->json([
                "error" => "error",
                "message" => "You don't have permissions to delete this post"
            ], 500);
    }

    public function upvote($id)
    {

        $post = Posts::find($id);
        try {

            $post->votes += 1;
            $post->save();

            return response()->json(['status' => 'Successfully upvoted post'], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => "error",
                "message" => "Unable to upvote a post"
            ], 500);
        }
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'image' => 'image',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error" => 'validation_errors',
                "message" => $validator->errors(),
            ], 422);
        }

        $user = auth()->user();
        if (!$user)
            return response()->json([
                "error" => "error",
                "message" => "Please tell us your name"
            ], 500);

        $image = false;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/images');
            $image_path = Storage::url($image);
        }

        try {



            $post = new Posts();
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->author = $user->id;
            if ($image)
                $post->image = $image_path;
            $post->save();

            return response()->json(['status' => 'Successfully added post'], 200);
        } catch (Exception $e) {
            return response()->json([
                "error" => "error",
                "message" => "Unable to add a post"
            ], 500);
        }
    }



}
