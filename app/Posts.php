<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Posts extends Model 
{
    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_edited';

    protected $table = 'posts';
    public $timestamps = false;
    protected $primary_key = 'id';

   
}
